[Latest version is there!](https://gitlab.com/tilluteenused/docker_elg_et_g2p_with_fork)


Don't use this one!

# Container with grapheme to phoneme rules for Estonian

Container (docker) of 
[grapheme to phoneme rules for Estonian](https://github.com/alumae/et-g2p) with interface compliant with 
[ELG requirements](https://european-language-grid.readthedocs.io/en/release1.0.0/all/LTInternalAPI.html#).

## Contains  <a name="Contains"></a>

* [Grapheme to phoneme rules for Estonian](https://github.com/alumae/et-g2p)
* Container and interface code

<!--
```commandline
sudo apt -y install ant 
sudo apt -y install openjdk-11-jdk
sudo update-alternatives --config java
java -version
```
-->

## Preliminaries

* You should have software for making / using the container installed; see instructions on the [docker web site](https://docs.docker.com/).
* In case you want to compile the code or build the container yourself, you should have version control software installed (see instructions on the [git web site](https://git-scm.com/)), and Java JDK (e.g. **_openjdk-11-jdk_**) .

## Downloading image from Docker Hub

You may dowload a ready-made container from Docker Hub, using the Linux command line (Windows / Mac command lines are similar):

```commandline
docker pull tilluteenused/et_g2p:2022.07.19
```
Next, continue to the section [Starting the container](#Starting_the_container).

## Making your own container

### 1. Downloading the source code

<!----
Lähtekood koosneb 2 osast
1. json liides, veebiserver ja konteineri tegemise asjad
2. Taneli asjad
----->

The source code comes from two sources.

#### 1.1  Downloading [container and interface code](https://gitlab.com/tilluteenused/docker_elg_et_g2p/) 

```commandline
mkdir -p ~/gitlab-docker-elg

cd ~/gitlab-docker-elg
git clone https://gitlab.com/tilluteenused/docker_elg_et_g2p gitlab_docker_et_g2p
```

#### 1.2 Downloading [grapheme to phoneme rules for Estonian](https://github.com/alumae/et-g2p) 

```commandline
cd gitlab_docker_et_g2p
git clone https://github.com/alumae/et-g2p.git github_et_g2p
```

### 2. Compiling and testing the code

**NB!** _openjdk-11-jdk_ must have been installed and running by default !!!

```commandline
cd ~/gitlab-docker-elg/gitlab_docker_et_g2p/github_et_g2p/
ant
ant test
```

### 3. Building the container

```commandline
cd ~/gitlab-docker-elg/gitlab_docker_et_g2p/
docker build -t tilluteenused/et_g2p:2022.07.19 .
```

## Starting the container <a name="Starting the container"></a>

```commandline
docker run -p 8000:8000 tilluteenused/et_g2p:2022.07.19
```

One need not be in a specific directory to start the container.

Ctrl+C in a terminal window with a running container in it will terminate the container.

## Query json

```json
{
  "type":"text",
  "content": string, /* list of tokens, delimited by space */
}
```

## Response json

```json
{
  "response":
  {
    "type":"texts",
    "texts":
    [             /* array of processed tokens */
      {
        "content": string, /* token */
        "features":
        {
          "analysis": [string, ...]
        }
      }
    ]
  }
}
```
## Usage example

```commandline
curl -i --request POST --header "Content-Type: application/json" --data '{"type":"text","content":"park OECD ABC-pood"}' localhost:8000/process
```

```json
HTTP/1.1 200 OK
Server: Werkzeug/2.1.2 Python/3.10.4
Date: Tue, 19 Jul 2022 15:21:41 GMT
Content-Type: application/json
Content-Length: 268
Connection: close

{"response":{"type":"texts","texts":[{"content":"park","features":{"analysis":["p a r kk"]}},{"content":"OECD","features":{"analysis":["o e k t","o o e e t s e e t e e"]}},{"content":"ABC-pood","features":{"analysis":["a p k p o o t","a a p e e t s e e p o o t"]}}]}}
```

## Sponsors

The container development was sponsored by EU CEF project [Microservices at your service](https://www.lingsoft.fi/en/microservices-at-your-service-bridging-gap-between-nlp-research-and-industry)


## Authors

Authors of the container: Tarmo Vaino, Heiki-Jaan Kaalep

Authors of the contents of the container: see references at section [Contains](#Contains).
 
