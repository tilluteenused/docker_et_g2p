# Eesti kirjakeelse sõna häälduspäraseks teisendamise reeglistikku sisaldav Docker'i konteiner

[Kirjakeelse sõna häälduspäraseks teisendamise reeglistikku](https://github.com/alumae/et-g2p) sisaldav tarkvara-konteiner (docker),
mille liides vastab [ELG nõuetele](https://european-language-grid.readthedocs.io/en/release1.0.0/all/LTInternalAPI.html#).

## Mida sisaldab <a name="Mida_sisaldab"></a>

* [Kirjakeelse sõna häälduspäraseks teisendamise reeglistik](https://github.com/alumae/et-g2p)
* Konteineri ja liidesega seotud lähtekood

## Eeltingimused

* Peab olema paigaldatud tarkvara konteineri tegemiseks/kasutamiseks; juhised on [docker'i veebilehel](https://docs.docker.com/).
* Kui sooviks on lähtekoodi ise kompileerida või konteinerit kokku panna, siis peab olema paigaldatud versioonihaldustarkvara (juhised on [git'i veebilehel](https://git-scm.com/)) ja
Java JDK (testitud **_openjdk-11-jdk_**'ga)
<!--
```commandline
sudo apt -y install openjdk-11-jdk
sudo update-alternatives --config java
java -version
sudo apt -y install ant 
```
-->

## Konteineri allalaadimine Docker Hub'ist

Valmis konteineri saab laadida alla Docker Hub'ist, kasutades Linux'i käsurida (Windows'i/Mac'i käsurida on analoogiline):

```commandline
docker pull tilluteenused/et_g2p:2022.07.19
```
 Seejärel saab jätkata osaga [Konteineri käivitamine](#Konteineri_käivitamine).

## Ise konteineri tegemine

### 1. Lähtekoodi allalaadimine

<!----
Lähtekood koosneb 2 osast
1. json liides, veebiserver ja konteineri tegemise asjad
2. Taneli asjad
----->

Lähtekood tuleb laadida alla kahest kohast.

#### 1.1 [Konteineri ja liidesega seotud lähtekoodi](https://gitlab.com/tilluteenused/docker_elg_et_g2p/) allalaadimine

```commandline
mkdir -p ~/gitlab-docker-elg

cd ~/gitlab-docker-elg
git clone https://gitlab.com/tilluteenused/docker_elg_et_g2p gitlab_docker_et_g2p
```

#### 1.2 [Kirjakeelse sõna häälduspäraseks teisendamise reeglistiku](https://github.com/alumae/et-g2p) lähtekoodi allalaadimine

```commandline
cd gitlab_docker_et_g2p
git clone https://github.com/alumae/et-g2p.git github_et_g2p
```

### 2. Lähtekoodi kompileerimine ja testimine

**NB!** Arvutis peab olema installitud ja vaikimisi töötama _openjdk-11-jdk_ !!!

```commandline
cd ~/gitlab-docker-elg/gitlab_docker_et_g2p/github_et_g2p/
ant
ant test
```

### 3. Konteineri kokkupanemine 

```commandline
cd ~/gitlab-docker-elg/gitlab_docker_et_g2p/
docker build -t tilluteenused/et_g2p:2022.07.19 .
```

## Konteineri käivitamine <a name="Konteineri_käivitamine"></a>

```commandline
docker run -p 8000:8000 tilluteenused/et_g2p:2022.07.19
```

Pole oluline, milline on jooksev kataloog terminaliaknas konteineri käivitamise hetkel.

Käivitatud konteineri töö lõpetab Ctrl+C selles terminaliaknas, kust konteiner käivitati.

## Päringu json-kuju

```json
{
  "type":"text",
  "content": string, /* tühikuga eraldatud sõnede loend */
}
```

## Vastuse json-kuju

```json
{
  "response":
  {
    "type":"texts",
    "texts":
    [             /* töödeldud sõnede massiiv */
      {
        "content": string, /* sõne */
        "features":
        {
          "analysis": [string, ...] /* häälduspärased kujud */
        }
      }
    ]
  }
}
```
## Kasutusnäide

```commandline
curl -i --request POST --header "Content-Type: application/json" --data '{"type":"text","content":"park OECD ABC-pood"}' localhost:8000/process
```

```json
HTTP/1.1 200 OK
Server: Werkzeug/2.1.2 Python/3.10.4
Date: Tue, 19 Jul 2022 15:21:41 GMT
Content-Type: application/json
Content-Length: 268
Connection: close

{"response":{"type":"texts","texts":[{"content":"park","features":{"analysis":["p a r kk"]}},{"content":"OECD","features":{"analysis":["o e k t","o o e e t s e e t e e"]}},{"content":"ABC-pood","features":{"analysis":["a p k p o o t","a a p e e t s e e p o o t"]}}]}}
```

## Rahastus

Konteiner loodi EL projekti [Microservices at your service](https://www.lingsoft.fi/en/microservices-at-your-service-bridging-gap-between-nlp-research-and-industry) toel.

## Autorid

Konteineri autorid: Tarmo Vaino, Heiki-Jaan Kaalep

Konteineri sisu autoreid vt. jaotises [Mida sisaldab](#Mida_sisaldab) toodud viidetest.
 
